﻿namespace TeamMembers.Shared.Models.Dtos
{
    public class TeamMemberResponse
    {
        public TeamMemberResponse(int id, string name, string email, string phone, bool? isActive, DateTime created)
        {
            Id = id;
            Name = name;
            Email = email;
            Phone = phone;
            IsActive = isActive;
            Created = created;
        }

        public int Id { get; }

        public string Name { get; }

        public string Email { get; }

        public string Phone { get; }

        public bool? IsActive { get; }

        public DateTime Created { get; }
    }
}