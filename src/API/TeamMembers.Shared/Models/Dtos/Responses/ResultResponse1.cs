﻿namespace TeamMembers.Shared.Models.Dtos
{
    public class ResultResponse<TModel> : ResultResponse
    {
        public TModel Value { get; } = default!;

        public ResultResponse(TModel value)
        {
            Value = value;
        }

        public static ResultResponse Ok<TResponseModel>(TResponseModel model) => new ResultResponse<TResponseModel>(model);
    }
}