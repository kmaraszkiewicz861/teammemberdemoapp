﻿namespace TeamMembers.Shared.Models.Dtos
{
    public class ResultResponse
    {
        public bool IsValid => ErrorMessages.Count() == 0;

        public IEnumerable<string> ErrorMessages { get; } = Enumerable.Empty<string>();

        protected ResultResponse()
        {

        }

        protected ResultResponse(IEnumerable<string> errorMessages)
        {
            ErrorMessages = errorMessages;
        }

        public static ResultResponse Ok() => new ResultResponse();

        public static ResultResponse Fail(IEnumerable<string> errorMessages) => new ResultResponse(errorMessages);
    }
}