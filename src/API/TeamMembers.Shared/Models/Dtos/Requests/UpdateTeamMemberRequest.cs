﻿namespace TeamMembers.Shared.Models.Dtos.Requests
{
    public class UpdateTeamMemberRequest
    {
        public UpdateTeamMemberRequest(int id, string name, string email, string phone)
        {
            Id = id;
            Name = name;
            Email = email;
            Phone = phone;
        }

        public int Id { get; set; }

        public string Name { get; }

        public string Email { get; }

        public string Phone { get; }
    }
}