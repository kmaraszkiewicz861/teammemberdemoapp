﻿namespace TeamMembers.Shared.Models.Dtos.Requests
{
    public sealed class GetTeamMemberDetailsRequest
    {
        public int Id { get; set; }
    }
}