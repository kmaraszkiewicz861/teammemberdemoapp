﻿namespace TeamMembers.Shared.Models.Dtos.Requests
{
    public class ChangeActiveStateRequest
    {
        public ChangeActiveStateRequest(int id, bool isActive)
        {
            Id = id;
            IsActive = isActive;
        }

        public int Id { get; }

        public bool IsActive { get; }
    }
}