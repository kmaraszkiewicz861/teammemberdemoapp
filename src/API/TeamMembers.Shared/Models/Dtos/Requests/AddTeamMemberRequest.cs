﻿namespace TeamMembers.Shared.Models.Dtos.Requests
{
    public sealed class AddTeamMemberRequest
    {
        public AddTeamMemberRequest(string name, string email, string phone)
        {
            Name = name;
            Email = email;
            Phone = phone;
        }

        public string Name { get; }

        public string Email { get; }

        public string Phone { get; }

        public bool IsActive => true;

        public DateTime Created => DateTime.UtcNow;
    }
}