﻿using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using TeamMembers.Application.Handlers.CommandHandlers;
using TeamMembers.Application.Validators;

namespace TeamMembers.Configuration
{
    public static class ApplicationLayerConfiguration
    {
        public static IServiceCollection RegisterApplicationLayerComponents(this IServiceCollection services)
        {
            services.AddMediatR(configuration =>
            {
                configuration.RegisterServicesFromAssembly(typeof(AddTeamMemberCommandHandler).Assembly);
            });

            services.AddValidatorsFromAssemblyContaining<TeamMemberRequestValidator>();

            return services;
        }
    }
}
