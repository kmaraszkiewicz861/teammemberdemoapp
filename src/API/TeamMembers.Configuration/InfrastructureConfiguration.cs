﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TeamMembers.Infrastructure.Database;
using TeamMembers.Infrastructure.Database.Queries;
using TeamMembers.Infrastructure.Database.Repository;

namespace TeamMembers.Configuration
{
    public static class InfrastructureLayerConfiguration
    {
        public static IServiceCollection RegisterInrastructureLayerComponents(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<TeamMembersDbContext>(options =>
            {
                options.UseNpgsql(connectionString);
            });

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ITeamMemberRepository, TeamMemberRepository>();
            services.AddScoped<ITeamMemberQuery, TeamMemberQuery>();

            return services;
        }
    }
}
