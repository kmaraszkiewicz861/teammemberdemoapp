﻿using FluentResults;
using FluentValidation.Results;

namespace TeamMembers.Application.Extensions
{
    internal static class ResultExtension
    {
        public static Result GenerateResult(this ValidationResult validationResult)
            => Result.Fail(validationResult.Errors?.Select(e => e.ErrorMessage)
                    ?? Enumerable.Empty<string>());
    }
}
