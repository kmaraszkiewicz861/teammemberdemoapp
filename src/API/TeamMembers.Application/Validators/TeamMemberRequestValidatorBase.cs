﻿using System.Text.RegularExpressions;
using FluentValidation;

namespace TeamMembers.Application.Validators
{
    public abstract class TeamMemberRequestValidatorBase<TRequest> : AbstractValidator<TRequest>
    {
        protected bool BeValidPhoneNumber(string phoneNumber)
        {
            string phonePattern = @"^\+\d{1,4}(?:\s?)\d{3}(?:\s?)\d{3}(?:\s?)\d{3}$";

            return Regex.IsMatch(phoneNumber, phonePattern);
        }
    }
}
