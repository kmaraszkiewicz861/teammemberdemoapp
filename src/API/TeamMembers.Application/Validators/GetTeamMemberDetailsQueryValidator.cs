﻿using FluentValidation;
using TeamMembers.Application.Handlers.Queries;

namespace TeamMembers.Application.Validators
{
    public sealed class GetTeamMemberDetailsQueryValidator : AbstractValidator<GetTeamMemberDetailsQuery>
    {
        public GetTeamMemberDetailsQueryValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty().WithMessage("Id cannot be empty");
        }
    }
}
