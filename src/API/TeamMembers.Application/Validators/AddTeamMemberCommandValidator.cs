﻿using FluentValidation;
using TeamMembers.Shared.Models.Dtos.Requests;

namespace TeamMembers.Application.Validators
{
    public sealed class TeamMemberRequestValidator : TeamMemberRequestValidatorBase<AddTeamMemberRequest>
    {
        public TeamMemberRequestValidator()
        {
            RuleFor(x => x.Name)
                .MaximumLength(256).WithMessage("Name should have less then 256 characters")
                .NotEmpty().WithMessage("Name cannot be empty");

            RuleFor(x => x.Email)
                .MaximumLength(256).WithMessage("Email should have less then 256 characters")
                .NotEmpty().WithMessage("Email cannot be empty")
                .EmailAddress().WithMessage("Invalid email address format.");

            RuleFor(x => x.Phone)
                .MaximumLength(24).WithMessage("Phone should have less then 256 characters")
                .NotEmpty().WithMessage("Phone cannot be empty")
                .Must(BeValidPhoneNumber).WithMessage("Invalid phone number format.");

        }
    }
}
