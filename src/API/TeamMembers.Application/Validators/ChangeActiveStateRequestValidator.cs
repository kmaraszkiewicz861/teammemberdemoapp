﻿using FluentValidation;
using TeamMembers.Shared.Models.Dtos.Requests;

namespace TeamMembers.Application.Validators
{
    public sealed class ChangeActiveStateRequestValidator : AbstractValidator<ChangeActiveStateRequest>
    {
        public ChangeActiveStateRequestValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty().WithMessage("Id cannot be empty");

        }
    }
}
