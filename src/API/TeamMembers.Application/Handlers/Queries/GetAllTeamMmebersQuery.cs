﻿using MediatR;
using TeamMembers.Shared.Models.Dtos;

namespace TeamMembers.Application.Handlers.Queries
{
    public sealed class GetAllTeamMembersQuery : IRequest<TeamMemberResponse[]>
    {
    }
}
