﻿using FluentResults;
using MediatR;
using TeamMembers.Shared.Models.Dtos;

namespace TeamMembers.Application.Handlers.Queries
{
    public sealed class GetTeamMemberDetailsQuery : IRequest<Result<TeamMemberResponse>>
    {
        public int Id { get; }

        public GetTeamMemberDetailsQuery(int id)
        {
            Id = id;
        }
    }
}
