﻿using FluentResults;
using FluentValidation;
using FluentValidation.Results;
using MediatR;
using TeamMembers.Application.Extensions;
using TeamMembers.Application.Handlers.Commands;
using TeamMembers.Infrastructure.Database.Repository;
using TeamMembers.Shared.Models.Dtos.Requests;

namespace TeamMembers.Application.Handlers.CommandHandlers
{
    public sealed class ChangeActiveStateCommandHandler : IRequestHandler<ChangeActiveStateCommand, Result>
    {
        private readonly IValidator<ChangeActiveStateRequest> _validator;

        private readonly ITeamMemberRepository _repository;

        private IUnitOfWork _unitOfWork;

        public ChangeActiveStateCommandHandler(IValidator<ChangeActiveStateRequest> validator, ITeamMemberRepository repository, IUnitOfWork unitOfWork)
        {
            _validator = validator;
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result> Handle(ChangeActiveStateCommand command, CancellationToken cancellationToken)
        {
            ValidationResult validationResult = await _validator.ValidateAsync(command.Request);

            if (!validationResult.IsValid)
            {
                return validationResult.GenerateResult();
            }

            Result result = await _repository.ChangeActiveStateAsync(command.Request);

            if (result.IsFailed)
            {
                return result;
            }

            await _unitOfWork.SaveChangesAsync();

            return Result.Ok();
        }
    }
}
