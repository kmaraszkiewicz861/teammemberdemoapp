﻿using FluentResults;
using FluentValidation;
using FluentValidation.Results;
using MediatR;
using TeamMembers.Application.Extensions;
using TeamMembers.Application.Handlers.Commands;
using TeamMembers.Infrastructure.Database.Repository;
using TeamMembers.Shared.Models.Dtos.Requests;

namespace TeamMembers.Application.Handlers.CommandHandlers
{
    public sealed class AddTeamMemberCommandHandler : IRequestHandler<AddTeamMemberCommand, Result>
    {
        private readonly IValidator<AddTeamMemberRequest> _validator;

        private readonly ITeamMemberRepository _repository;

        private IUnitOfWork _unitOfWork;

        public AddTeamMemberCommandHandler(IValidator<AddTeamMemberRequest> validator, ITeamMemberRepository repository, IUnitOfWork unitOfWork)
        {
            _validator = validator;
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        public async Task<Result> Handle(AddTeamMemberCommand command, CancellationToken cancellationToken)
        {
            ValidationResult result = await _validator.ValidateAsync(command.Request);

            if (!result.IsValid)
            {
                return result.GenerateResult();
            }

            await _repository.AddAsync(command.Request);

            await _unitOfWork.SaveChangesAsync();

            return Result.Ok();
        }
    }
}
