﻿using FluentResults;
using FluentValidation;
using FluentValidation.Results;
using MediatR;
using TeamMembers.Application.Extensions;
using TeamMembers.Application.Handlers.Queries;
using TeamMembers.Infrastructure.Database.Queries;
using TeamMembers.Shared.Models.Dtos;

namespace TeamMembers.Application.Handlers.QueryHandlers
{
    public sealed class GetTeamMemberDetailsQueryHandler : IRequestHandler<GetTeamMemberDetailsQuery, Result<TeamMemberResponse>>
    {
        private readonly ITeamMemberQuery _teamMemberQuery;

        private readonly IValidator<GetTeamMemberDetailsQuery> _validator;

        public GetTeamMemberDetailsQueryHandler(ITeamMemberQuery teamMemberQuery, IValidator<GetTeamMemberDetailsQuery> validator)
        {
            _teamMemberQuery = teamMemberQuery;
            _validator = validator;
        }

        public async Task<Result<TeamMemberResponse>> Handle(GetTeamMemberDetailsQuery request, CancellationToken cancellationToken)
        {
            ValidationResult result = await _validator.ValidateAsync(request);

            if (!result.IsValid)
            {
                return result.GenerateResult();
            }

            return await _teamMemberQuery.GetDetailsAsync(request.Id);
        }
    }
}
