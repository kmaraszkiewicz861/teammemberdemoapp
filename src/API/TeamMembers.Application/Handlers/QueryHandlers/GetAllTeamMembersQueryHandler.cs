﻿using MediatR;
using TeamMembers.Application.Handlers.Queries;
using TeamMembers.Infrastructure.Database.Queries;
using TeamMembers.Shared.Models.Dtos;

namespace TeamMembers.Application.Handlers.QueryHandlers
{
    public sealed class GetAllTeamMembersQueryHandler : IRequestHandler<GetAllTeamMembersQuery, TeamMemberResponse[]>
    {
        private readonly ITeamMemberQuery _teamMemberQuery;

        public GetAllTeamMembersQueryHandler(ITeamMemberQuery teamMemberQuery)
        {
            _teamMemberQuery = teamMemberQuery;
        }

        public async Task<TeamMemberResponse[]> Handle(GetAllTeamMembersQuery request, CancellationToken cancellationToken)
            => await _teamMemberQuery.GetAllAsync();
    }
}
