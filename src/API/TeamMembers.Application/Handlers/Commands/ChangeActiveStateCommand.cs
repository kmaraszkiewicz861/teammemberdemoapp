﻿using FluentResults;
using MediatR;
using TeamMembers.Shared.Models.Dtos.Requests;

namespace TeamMembers.Application.Handlers.Commands
{
    public sealed class ChangeActiveStateCommand : IRequest<Result>
    {
        public ChangeActiveStateRequest Request { get; set; }

        public ChangeActiveStateCommand(ChangeActiveStateRequest request)
        {
            Request = request;
        }
    }
}
