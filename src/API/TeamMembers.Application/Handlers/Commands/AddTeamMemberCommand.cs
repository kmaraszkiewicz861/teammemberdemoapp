﻿using FluentResults;
using MediatR;
using TeamMembers.Shared.Models.Dtos.Requests;

namespace TeamMembers.Application.Handlers.Commands
{
    public sealed class AddTeamMemberCommand : IRequest<Result>
    {
        public AddTeamMemberRequest Request { get; }

        public AddTeamMemberCommand(AddTeamMemberRequest request)
        {
            Request = request;
        }
    }
}
