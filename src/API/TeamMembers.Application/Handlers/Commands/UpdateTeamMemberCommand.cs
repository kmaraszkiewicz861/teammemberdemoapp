﻿using FluentResults;
using MediatR;
using TeamMembers.Shared.Models.Dtos.Requests;

namespace TeamMembers.Application.Handlers.Commands
{
    public sealed class UpdateTeamMemberCommand : IRequest<Result>
    {
        public UpdateTeamMemberRequest Request { get; set; }

        public UpdateTeamMemberCommand(UpdateTeamMemberRequest request)
        {
            Request = request;
        }
    }
}
