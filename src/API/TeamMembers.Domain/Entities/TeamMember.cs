﻿namespace TeamMembers.Domain.Entities
{
    public class TeamMember
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public byte[]? Photo { get; set; }

        public bool? IsActive { get; set; }

        public DateTime Created { get; set; }
    }
}
