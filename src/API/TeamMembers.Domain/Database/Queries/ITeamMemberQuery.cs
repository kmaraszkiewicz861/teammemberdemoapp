﻿using FluentResults;
using TeamMembers.Shared.Models.Dtos;

namespace TeamMembers.Infrastructure.Database.Queries
{
    public interface ITeamMemberQuery
    {
        Task<TeamMemberResponse[]> GetAllAsync();
        Task<Result<TeamMemberResponse>> GetDetailsAsync(int id);
    }
}
