﻿using FluentResults;
using TeamMembers.Shared.Models.Dtos.Requests;

namespace TeamMembers.Infrastructure.Database.Repository
{
    public interface IUnitOfWork
    {
        Task SaveChangesAsync();
    }

    public interface ITeamMemberRepository
    {
        Task AddAsync(AddTeamMemberRequest teamMemberDto);
        Task<Result> ChangeActiveStateAsync(ChangeActiveStateRequest request);
        Task<Result> UpdateAsync(UpdateTeamMemberRequest request);
    }
}
