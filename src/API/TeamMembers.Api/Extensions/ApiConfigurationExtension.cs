﻿namespace TeamMembers.Api.Extensions
{
    public static class ApiConfigurationExtension
    {
        public static void ConfigureCors(this IServiceCollection services)
        {
            services.AddCors(setup =>
            {
                setup.AddDefaultPolicy(policy =>
                {
                    policy.WithOrigins("http://localhost:3000")
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials();
                });
            });
        }
    }
}
