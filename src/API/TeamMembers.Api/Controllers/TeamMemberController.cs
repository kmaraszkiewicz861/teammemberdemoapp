using FluentResults;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using TeamMembers.Application.Handlers.Commands;
using TeamMembers.Application.Handlers.Queries;
using TeamMembers.Shared.Models.Dtos;
using TeamMembers.Shared.Models.Dtos.Requests;

namespace TeamMembers.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public sealed class TeamMemberController : ControllerBase
    {
        private readonly IMediator _mediator;

        public TeamMemberController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<TeamMemberResponse[]>> GetAsync() 
            => Ok(await _mediator.Send(new GetAllTeamMembersQuery { }));

        [HttpGet("id:int")]
        public async Task<ActionResult<Result<TeamMemberResponse>>> GetAsync([FromQuery] GetTeamMemberDetailsRequest request)
        {
            Result<TeamMemberResponse> result = await _mediator.Send(new GetTeamMemberDetailsQuery(request.Id));

            if (result.IsFailed)
            {
                return BadRequest(ResultResponse.Fail(result.Errors?.Select(e => e.Message)
                                                        ?? Enumerable.Empty<string>()));
            }

            return Ok(ResultResponse<TeamMemberResponse>.Ok(result.Value));
        }

        [HttpPost]
        public async Task<ActionResult<ResultResponse>> AddAsync(AddTeamMemberRequest request)
        {
            Result result = await _mediator.Send(new AddTeamMemberCommand(request));

            return GenerateResponse(result);
        }

        [HttpPut]
        public async Task<ActionResult<ResultResponse>> UpdateAsync(UpdateTeamMemberRequest request)
        {
            Result result = await _mediator.Send(new UpdateTeamMemberCommand(request));

            return GenerateResponse(result);
        }

        [HttpPatch]
        public async Task<ActionResult<ResultResponse>> ChangeActiveStateAsync(ChangeActiveStateRequest request)
        {
            Result result = await _mediator.Send(new ChangeActiveStateCommand(request));

            return GenerateResponse(result);
        }

        private ActionResult<ResultResponse> GenerateResponse(Result result)
        {
            if (result.IsFailed)
            {
                return BadRequest(ResultResponse.Fail(result.Errors?.Select(e => e.Message) 
                                                        ?? Enumerable.Empty<string>()));
            }

            return Ok(ResultResponse.Ok());
        }
    }
}
