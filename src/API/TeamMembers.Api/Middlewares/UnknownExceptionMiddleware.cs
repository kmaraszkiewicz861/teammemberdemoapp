﻿using System.Text.Json;
using FluentResults;

namespace TeamMembers.Api.Middlewares
{
    public class UnknownExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public UnknownExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                var error = Result.Fail("An unexpected error occurred.");
                string errorJson = JsonSerializer.Serialize(error);

                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                context.Response.ContentType = "application/json";

                await context.Response.WriteAsync(errorJson);
            }
        }
    }

}
