﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TeamMembers.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class add_phone_column_to_teammember : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "team_member",
                type: "character varying(128)",
                maxLength: 128,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(256)",
                oldMaxLength: 256);

            migrationBuilder.AddColumn<string>(
                name: "Phone",
                table: "team_member",
                type: "character varying(24)",
                maxLength: 24,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<byte[]>(
                name: "Photo",
                table: "team_member",
                type: "bytea",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Created", "Phone", "Photo" },
                values: new object[] { new DateTime(2024, 1, 8, 14, 49, 13, 527, DateTimeKind.Utc).AddTicks(6146), "+48 123 456 789", null });

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created", "IsActive", "Phone", "Photo" },
                values: new object[] { new DateTime(2024, 1, 8, 14, 49, 13, 527, DateTimeKind.Utc).AddTicks(6151), false, "+48 123 456 789", null });

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Created", "Phone", "Photo" },
                values: new object[] { new DateTime(2024, 1, 8, 14, 49, 13, 527, DateTimeKind.Utc).AddTicks(6153), "+48 123 456 789", null });

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Created", "IsActive", "Phone", "Photo" },
                values: new object[] { new DateTime(2024, 1, 8, 14, 49, 13, 527, DateTimeKind.Utc).AddTicks(6154), false, "+48 123 456 789", null });

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Created", "Phone", "Photo" },
                values: new object[] { new DateTime(2024, 1, 8, 14, 49, 13, 527, DateTimeKind.Utc).AddTicks(6156), "+48 123 456 789", null });

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Created", "IsActive", "Phone", "Photo" },
                values: new object[] { new DateTime(2024, 1, 8, 14, 49, 13, 527, DateTimeKind.Utc).AddTicks(6158), false, "+48 123 456 789", null });

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Created", "Phone", "Photo" },
                values: new object[] { new DateTime(2024, 1, 8, 14, 49, 13, 527, DateTimeKind.Utc).AddTicks(6160), "+48 123 456 789", null });

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Created", "IsActive", "Phone", "Photo" },
                values: new object[] { new DateTime(2024, 1, 8, 14, 49, 13, 527, DateTimeKind.Utc).AddTicks(6161), false, "+48 123 456 789", null });

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "Created", "Phone", "Photo" },
                values: new object[] { new DateTime(2024, 1, 8, 14, 49, 13, 527, DateTimeKind.Utc).AddTicks(6162), "+48 123 456 789", null });

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Created", "IsActive", "Phone", "Photo" },
                values: new object[] { new DateTime(2024, 1, 8, 14, 49, 13, 527, DateTimeKind.Utc).AddTicks(6165), false, "+48 123 456 789", null });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Phone",
                table: "team_member");

            migrationBuilder.DropColumn(
                name: "Photo",
                table: "team_member");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "team_member",
                type: "character varying(256)",
                maxLength: 256,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(128)",
                oldMaxLength: 128);

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 1,
                column: "Created",
                value: new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(995));

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Created", "IsActive" },
                values: new object[] { new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(1000), true });

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 3,
                column: "Created",
                value: new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(1002));

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Created", "IsActive" },
                values: new object[] { new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(1004), true });

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 5,
                column: "Created",
                value: new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(1005));

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Created", "IsActive" },
                values: new object[] { new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(1007), true });

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 7,
                column: "Created",
                value: new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(1008));

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Created", "IsActive" },
                values: new object[] { new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(1010), true });

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 9,
                column: "Created",
                value: new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(1039));

            migrationBuilder.UpdateData(
                table: "team_member",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "Created", "IsActive" },
                values: new object[] { new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(1042), true });
        }
    }
}
