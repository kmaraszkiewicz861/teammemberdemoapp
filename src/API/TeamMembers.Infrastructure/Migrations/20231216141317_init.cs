﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace TeamMembers.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "team_member",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: false),
                    Email = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: false),
                    IsActive = table.Column<bool>(type: "boolean", nullable: true, defaultValue: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_team_member", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "team_member",
                columns: new[] { "Id", "Created", "Email", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(995), "john.doe1@example.com", "John Doe 1" },
                    { 2, new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(1000), "john.doe2@example.com", "John Doe 2" },
                    { 3, new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(1002), "john.doe3@example.com", "John Doe 3" },
                    { 4, new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(1004), "john.doe4@example.com", "John Doe 4" },
                    { 5, new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(1005), "john.doe5@example.com", "John Doe 5" },
                    { 6, new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(1007), "john.doe6@example.com", "John Doe 6" },
                    { 7, new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(1008), "john.doe7@example.com", "John Doe 7" },
                    { 8, new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(1010), "john.doe8@example.com", "John Doe 8" },
                    { 9, new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(1039), "john.doe9@example.com", "John Doe 9" },
                    { 10, new DateTime(2023, 12, 16, 14, 13, 17, 526, DateTimeKind.Utc).AddTicks(1042), "john.doe10@example.com", "John Doe 10" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "team_member");
        }
    }
}
