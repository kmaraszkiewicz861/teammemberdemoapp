﻿using Riok.Mapperly.Abstractions;
using TeamMembers.Domain.Entities;
using TeamMembers.Shared.Models.Dtos;

namespace TeamMembers.Infrastructure.Mappers
{
    [Mapper]
    public partial class TeamMemberMapper
    {
        public partial TeamMemberResponse ToTeamMemberDto(TeamMember teamMember);
        public partial TeamMemberResponse[] ToTeamMemberDtos(TeamMember[] teamMembers);
    }
}
