﻿using Riok.Mapperly.Abstractions;
using TeamMembers.Domain.Entities;
using TeamMembers.Shared.Models.Dtos;
using TeamMembers.Shared.Models.Dtos.Requests;

namespace TeamMembers.Infrastructure.Mappers
{
    [Mapper]
    public partial class TeamMemberDtoMapper
    {
        public partial TeamMember ToTeamMember(AddTeamMemberRequest teamMember);
    }
}
