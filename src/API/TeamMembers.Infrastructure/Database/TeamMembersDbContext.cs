﻿using Microsoft.EntityFrameworkCore;
using TeamMembers.Domain.Entities;
using TeamMembers.Infrastructure.Database.Configurations;

namespace TeamMembers.Infrastructure.Database
{
    public class TeamMembersDbContext : DbContext
    {
        public TeamMembersDbContext(DbContextOptions options) : base(options)
        {
            
        }

        public DbSet<TeamMember> TeamMembers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new TeamMemberConfigurations());

            base.OnModelCreating(modelBuilder);
        }
    }
}
