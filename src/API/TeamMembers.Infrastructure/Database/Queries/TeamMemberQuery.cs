﻿using FluentResults;
using Microsoft.EntityFrameworkCore;
using TeamMembers.Domain.Entities;
using TeamMembers.Infrastructure.Mappers;
using TeamMembers.Shared.Models.Dtos;

namespace TeamMembers.Infrastructure.Database.Queries
{
    public sealed class TeamMemberQuery : ITeamMemberQuery
    {
        private readonly TeamMembersDbContext _context;

        public TeamMemberQuery(TeamMembersDbContext context)
        {
            _context = context;
        }

        public async Task<Result<TeamMemberResponse>> GetDetailsAsync(int id)
        {
            TeamMember? teamMember = await _context.TeamMembers.FindAsync(id);

            if (teamMember is null)
            {
                return Result.Fail<TeamMemberResponse>("Team member not found");
            }

            TeamMemberMapper mapper = new();

            return mapper.ToTeamMemberDto(teamMember);
        }

        public async Task<TeamMemberResponse[]> GetAllAsync()
        {
            TeamMember[] teamMembers = await _context
                .TeamMembers
                .OrderBy(x => x.Name)
                .ToArrayAsync();

            TeamMemberMapper mapper = new();

            return mapper.ToTeamMemberDtos(teamMembers);
        }
    }
}
