﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace TeamMembers.Infrastructure.Database
{
    public class TeamMembersDbContextFactory : IDesignTimeDbContextFactory<TeamMembersDbContext>
    {
        public TeamMembersDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<TeamMembersDbContext>();
            optionsBuilder.UseNpgsql("Server=localhost;Port=5432;Database=TeamMembers;User Id=postgres;Password=postgreSSDemo!@#;");

            return new TeamMembersDbContext(optionsBuilder.Options);
        }
    }
}
