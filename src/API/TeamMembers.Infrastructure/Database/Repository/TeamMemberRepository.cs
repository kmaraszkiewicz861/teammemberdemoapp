﻿using FluentResults;
using TeamMembers.Domain.Entities;
using TeamMembers.Infrastructure.Mappers;
using TeamMembers.Shared.Models.Dtos.Requests;

namespace TeamMembers.Infrastructure.Database.Repository
{
    public sealed class TeamMemberRepository : ITeamMemberRepository
    {
        private readonly TeamMembersDbContext _context;

        public TeamMemberRepository(TeamMembersDbContext context)
        {
            _context = context;
        }

        public async Task AddAsync(AddTeamMemberRequest teamMemberDto)
        {
            TeamMemberDtoMapper mapper = new();

            TeamMember teamMember = mapper.ToTeamMember(teamMemberDto);

            await _context.AddAsync(teamMember);
        }

        public async Task<Result> UpdateAsync(UpdateTeamMemberRequest request)
        {
            TeamMember? teamMember = await _context.TeamMembers.FindAsync(request.Id);

            if (teamMember is null)
                return Result.Fail($"Memeber not found!");

            teamMember.Name = request.Name;
            teamMember.Email = request.Email;
            teamMember.Phone = request.Phone;

            _context.TeamMembers.Update(teamMember);

            return Result.Ok();
        }

        public async Task<Result> ChangeActiveStateAsync(ChangeActiveStateRequest request)
        {
            TeamMember? teamMember = await _context.TeamMembers.FindAsync(request.Id);

            if (teamMember is null)
                return Result.Fail($"Memeber not found!");

            teamMember.IsActive = request.IsActive;

            _context.TeamMembers.Update(teamMember);

            return Result.Ok();
        }
    }
}
