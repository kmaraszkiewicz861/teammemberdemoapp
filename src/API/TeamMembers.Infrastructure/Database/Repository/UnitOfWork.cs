﻿namespace TeamMembers.Infrastructure.Database.Repository
{
    public sealed class UnitOfWork : IUnitOfWork
    {
        private readonly TeamMembersDbContext _context;

        public UnitOfWork(TeamMembersDbContext context)
        {
            _context = context;
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
