﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TeamMembers.Domain.Entities;

namespace TeamMembers.Infrastructure.Database.Configurations
{
    internal class TeamMemberConfigurations : IEntityTypeConfiguration<TeamMember>
    {
        public void Configure(EntityTypeBuilder<TeamMember> builder)
        {
            builder.ToTable("team_member");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name)
                .HasMaxLength(256)
                .IsRequired();

            builder.Property(x => x.Email)
                .HasMaxLength(128)
                .IsRequired();

            builder.Property(x => x.Phone)
                .HasMaxLength(24)
                .IsRequired();

            builder.Property(x => x.IsActive)
                .HasDefaultValue(true);

            builder.Property(x => x.Created)
                .IsRequired();

            SeedData(builder);
        }

        public static void SeedData(EntityTypeBuilder<TeamMember> builder)
        {
            List<TeamMember> teamMembers = new();

            for (var index = 1; index <= 10; index++)
            {
                teamMembers.Add(new TeamMember
                {
                    Id = index,
                    Name = $"John Doe {index}",
                    Email = $"john.doe{index}@example.com",
                    Phone = "+48 123 456 789",
                    IsActive = (index % 2 == 1),
                    Created = DateTime.UtcNow,
                });
            }

            builder.HasData(teamMembers);
        }
    }
}
