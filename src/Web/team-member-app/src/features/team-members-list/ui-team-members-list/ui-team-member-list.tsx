import { ChangeMemberState, TeamMemeber } from '../../../shared/types/team-members-types';
import { UpdateTeamMemberFormData } from '../../update-team-member/update-team-member-form-types';
import './ui-team-member-list.scss';

interface UiTeamMemberListProps {
    teamMembers: TeamMemeber[],
    onAddMemberButton: () => void;
    onClikcEditButton: (teamMember: UpdateTeamMemberFormData) => void;
    onClickChangeStatus: (state: ChangeMemberState) => void;
}

function UiTeamMemberList({teamMembers, onAddMemberButton, onClikcEditButton, onClickChangeStatus}: UiTeamMemberListProps) {

    const handleClickEditButton = (teamMember: TeamMemeber) => {
      onClikcEditButton({
        id: teamMember.id,
        name: teamMember.name,
        phone: teamMember.phone,
        email: teamMember.email
      });
    }

    const handleClickChangeStatusButton = (id: number, isActive: boolean) => {
      const newIsActiveValue = !isActive;
      
      onClickChangeStatus({
        id,
        isActive: newIsActiveValue
      });
    }

    const getStatusButtonText = (isActive: boolean): string => {
      return isActive ? 'Disable' : 'Enable';
    }

    return (<div className='team-members-list-container'>
        <div className="header-container">
          <div className='title-container'>
            <span className="main-title-text" >Lista członków zespołu</span>
            <span className="bottom-title-text" >Zarządzaj listą członków swojego zespołu</span>
          </div>
          <button className="add-team-member-button" onClick={onAddMemberButton} >Add new team member</button>
        </div>

        <div className='team-member-table'>
            <div className="row header">
              <div className="header-cell">Nazwa</div>
              <div className="header-cell">Adres e-mail</div>
              <div className="header-cell">Numer telefonu</div>
              <div className="header-cell">Status</div>
              <div className="header-cell">Data utworzenia</div>
              <div className="header-cell">Akcje</div>
            </div>
            {teamMembers.map((teamMember) => (      
              <div className='row'>
                <div className='cell'>
                  {teamMember.name}
                </div>
                <div className='cell'>
                  {teamMember.email}
                </div>
                <div className='cell'>
                  {teamMember.phone}
                </div>
                <div className='cell'>
                  {teamMember.isActive.toString()}
                </div>
                <div className='cell'>
                  {teamMember.created}
                </div>
                <div className='cell'>
                  <button onClick={() => handleClickEditButton(teamMember)}>Edit</button>
                  <button onClick={() => handleClickChangeStatusButton(
                                            teamMember.id, 
                                            teamMember.isActive)}>
                    {getStatusButtonText(teamMember.isActive)}
                  </button>
                </div>
              </div>
          ))}
          </div>

        
      </div>)
}

export default UiTeamMemberList;