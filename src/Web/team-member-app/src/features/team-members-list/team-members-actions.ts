import { Dispatch } from '@reduxjs/toolkit';
import { ChangeMemberState, TeamMemeber } from '../../shared/types/team-members-types';
import { submitFormError, submitFormRequest, submitFormSuccess } from '../../shared/events/reducer-events';
import axiosInstance from '../../core/axios-instance';
import { getErrorFromAxiosResponse } from '../../shared/helpers/axios-error-helpers';

export const FETCH_DATA_REQUEST = 'FETCH_DATA_REQUEST';
export const FETCH_DATA_SUCCESS = 'FETCH_DATA_SUCCESS';
export const FETCH_DATA_ERROR = 'FETCH_DATA_ERROR';

export const fetchDataRequest = () => ({
    type: FETCH_DATA_REQUEST,
});
  
export const fetchDataSuccess = (data: any) => ({
    type: FETCH_DATA_SUCCESS,
    payload: data,
});
  
export const fetchDataError = (error: string) => ({
    type: FETCH_DATA_ERROR,
    payload: error,
});

export const fetchTeamMembers = () => {
    return async (dispatch: Dispatch) => {
        try {
            dispatch(fetchDataRequest());
            const response = await axiosInstance.get<TeamMemeber[]>('/api/TeamMember');
            dispatch(fetchDataSuccess(response.data));
        } catch (error) {
            const errorMessage = (error as { message: string }).message;
            dispatch(fetchDataError(errorMessage));
        }
    }
}

export const changeMemberStatus = (formData: ChangeMemberState) => {
    return async (dispatch: Dispatch) => {
        dispatch(submitFormRequest());
    
        try {
            const response = await axiosInstance.patch('/api/TeamMember', formData);
            dispatch(submitFormSuccess(response.data));
        } catch (error) {
            dispatch(submitFormError(getErrorFromAxiosResponse(error)));
        }
      };
}