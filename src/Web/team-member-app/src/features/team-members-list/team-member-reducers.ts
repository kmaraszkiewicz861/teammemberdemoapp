import { TeamMemeber } from '../../shared/types/team-members-types';
import { SUBMIT_FORM_ERROR, SUBMIT_FORM_REQUEST, SUBMIT_FORM_SUCCESS } from '../../shared/types/shared-consts';
import { FETCH_DATA_ERROR, FETCH_DATA_REQUEST, FETCH_DATA_SUCCESS } from './team-members-actions';

interface TeamMemberState {
    teamMembers: TeamMemeber[];
    loading: boolean;
    error: string | null;
}

const initialState: TeamMemberState = {
    teamMembers: [],
    loading: false,
    error: null
}

const teamMemberReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case FETCH_DATA_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        teamMembers: action.payload,
      };
    case FETCH_DATA_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

const changeMemberStatusReducer = (state = initialState, action: any) => {
    switch (action.type) {
      case SUBMIT_FORM_REQUEST:
        return {
          ...state,
          submitFormLoading: true,
          submitFormSuccess: null,
          submitFormError: null,
        };
      case SUBMIT_FORM_SUCCESS:
        return {
          ...state,
          submitFormLoading: false,
          submitFormSuccess: action.payload,
        };
      case SUBMIT_FORM_ERROR:
        return {
          ...state,
          submitFormLoading: false,
          submitFormError: action.payload,
        };
      default:
        return state;
    }
  };

export { teamMemberReducer, changeMemberStatusReducer }