import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../core/store';
import { changeMemberStatus, fetchTeamMembers } from './team-members-actions';
import UiTeamMemberList  from './ui-team-members-list/ui-team-member-list';
import AddTeamMemberForm from '../add-team-member/add-team-member-form';
import UpdateTeamMemberForm from '../update-team-member/update-team-member-form';
import { UpdateTeamMemberFormData } from '../update-team-member/update-team-member-form-types';
import { ChangeMemberState } from '../../shared/types/team-members-types';
import Modal from '../../shared/views/modal/modal';

const ADD_MODAL_TYPE: number = 1;
const EDIT_MODAL_TYPE: number = 2;
const STATUS_MODAL_TYPE: number = 3;

function TeamMembersList() {
    const dispatch = useDispatch<AppDispatch>();
    const { teamMembers, loading, error } = useSelector((state: RootState) => state.teamMembers);
    const [showAddModal, setAddShowModal] = useState(false);
    const [showEditModal, setEditShowModal] = useState(false);
    const [showStatusModal, setStatusShowModal] = useState(false);
    const [editItem, setEditItem] = useState<UpdateTeamMemberFormData>({
      id: -1,
      name: '',
      email: '',
      phone: '',
    });
    const [changeStatusFinishMessage, setChangeStatusFinishMessage] = useState('');

    useEffect(() => {
        dispatch(fetchTeamMembers());
    }, [dispatch])

    const handleModalClose = (modalType: number) => {

      switch (modalType) {
        case ADD_MODAL_TYPE:
          setAddShowModal(false);
          break;

        case EDIT_MODAL_TYPE:
          setEditShowModal(false);
          break;

        case STATUS_MODAL_TYPE:
          setStatusShowModal(false);
          break;
      }

      setAddShowModal(false);
    }

    const handleModalAddShow = () => {
      setAddShowModal(true);
    }

    const onClikcEditButton = (teamMember: UpdateTeamMemberFormData) => {
      setEditItem(teamMember);
      setEditShowModal(true);
    }

    const onClickChangeStatus = async (state: ChangeMemberState) => {
      await dispatch(changeMemberStatus(state));
      await dispatch(fetchTeamMembers());
      setChangeStatusFinishMessage(state.isActive ? 'Członek zepołu został doblokowany' : 'Członek zepołu został zablokowany');
      setStatusShowModal(true);
    }

    const onSucess = async () => {
      await dispatch(fetchTeamMembers());
    }

    if (loading) {
        return <div>Loading...</div>
    }

    if (error) {
        return <div>Error: {error}</div>
    }

    return (
    <div>
      <Modal show={showAddModal} handleClose={() => handleModalClose(ADD_MODAL_TYPE)} >
        <AddTeamMemberForm onSuccess={onSucess} />
      </Modal>

      <Modal show={showEditModal} handleClose={() => handleModalClose(EDIT_MODAL_TYPE)} >
        <UpdateTeamMemberForm data={editItem} onSuccess={onSucess} />
      </Modal>

      <Modal show={showStatusModal} handleClose={() => handleModalClose(STATUS_MODAL_TYPE)} >
        <span className='title-text'>Potwierdzenie</span>
        <span className='modal-message'>{changeStatusFinishMessage}</span>
      </Modal>
      
      <UiTeamMemberList onAddMemberButton={handleModalAddShow}
        onClikcEditButton={(teamMember: UpdateTeamMemberFormData) => onClikcEditButton(teamMember)} 
        onClickChangeStatus={(state: ChangeMemberState) => onClickChangeStatus(state)} 
        teamMembers={teamMembers} />
    </div>);
}

export default TeamMembersList;