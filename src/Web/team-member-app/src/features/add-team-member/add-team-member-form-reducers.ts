import { SUBMIT_FORM_ERROR, SUBMIT_FORM_REQUEST, SUBMIT_FORM_SUCCESS } from "../../shared/types/shared-consts";

const initialState = {
    formData: {},
    submitFormLoading: false,
    submitFormSuccess: null,
    submitFormError: null,
  };

  const submitAddTeamMemberFormReducer = (state = initialState, action: any) => {
    switch (action.type) {
      case SUBMIT_FORM_REQUEST:
        return {
          ...state,
          submitFormLoading: true,
          submitFormSuccess: null,
          submitFormError: null,
        };
      case SUBMIT_FORM_SUCCESS:
        return {
          ...state,
          submitFormLoading: false,
          submitFormSuccess: action.payload,
        };
      case SUBMIT_FORM_ERROR:
        return {
          ...state,
          submitFormLoading: false,
          submitFormError: action.payload,
        };
      default:
        return state;
    }
  };

  export default submitAddTeamMemberFormReducer;