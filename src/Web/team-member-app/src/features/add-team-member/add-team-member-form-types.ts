export interface AddTeamMemberFormData {
    name: string;
    email: string;
    phone: string;   
}