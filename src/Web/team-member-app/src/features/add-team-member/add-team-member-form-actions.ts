import { Dispatch } from 'redux';
import { AddTeamMemberFormData } from './add-team-member-form-types';
import axiosInstance from '../../core/axios-instance';
import { submitFormError, submitFormRequest, submitFormSuccess } from '../../shared/events/reducer-events';
import { getErrorFromAxiosResponse } from '../../shared/helpers/axios-error-helpers';

export const submitAddTeamMemberFrom = (formData: AddTeamMemberFormData) => {
    return async (dispatch: Dispatch) => {
        dispatch(submitFormRequest());
    
        try {
            const response = await axiosInstance.post('/api/TeamMember', formData);
            dispatch(submitFormSuccess(response.data));
        } catch (error) {
            dispatch(submitFormError(getErrorFromAxiosResponse(error)));
        }
      };
}