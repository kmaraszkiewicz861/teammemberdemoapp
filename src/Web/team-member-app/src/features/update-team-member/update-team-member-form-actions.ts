import { Dispatch } from 'redux';
import { UpdateTeamMemberFormData } from './update-team-member-form-types';
import axiosInstance from '../../core/axios-instance';
import { submitFormError, submitFormRequest, submitFormSuccess } from '../../shared/events/reducer-events';
import { getErrorFromAxiosResponse } from '../../shared/helpers/axios-error-helpers';

export const submitUpdateTeamMemberFrom = (formData: UpdateTeamMemberFormData) => {
    return async (dispatch: Dispatch) => {
        dispatch(submitFormRequest());
    
        try {
            const response = await axiosInstance.put('/api/TeamMember', formData);
            dispatch(submitFormSuccess(response.data));
        } catch (error) {
            dispatch(submitFormError(getErrorFromAxiosResponse(error)));
        }
      };
}