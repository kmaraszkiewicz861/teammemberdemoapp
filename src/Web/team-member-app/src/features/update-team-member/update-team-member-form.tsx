import React, { useState, useEffect } from 'react';
import { UpdateTeamMemberFormData } from './update-team-member-form-types';
import { submitUpdateTeamMemberFrom } from './update-team-member-form-actions';
import { useDispatch, useSelector } from 'react-redux';
import { AppDispatch, RootState } from '../../core/store';

interface UpdateTeamMemberFormProp {
    data: UpdateTeamMemberFormData;
    onSuccess: () => void;
} 

function UpdateTeamMemberForm({ data, onSuccess }: UpdateTeamMemberFormProp) {
    const dispatch = useDispatch<AppDispatch>();
    const submitFormLoading = useSelector<RootState, boolean>(state => state.submitAddTeamMemberForm.submitFormLoading);
    const submitFormSuccess = useSelector<RootState, boolean>(state => state.submitAddTeamMemberForm.submitFormSuccess);
    const submitFormError = useSelector<RootState, boolean>(state => state.submitAddTeamMemberForm.submitFormError);
  
    const [formData, setFormData] = useState<UpdateTeamMemberFormData>({
        id: -1,
        name: '',
        email: '',
        phone: '',
    });

    useEffect(() => {
       setFormData(data); 
    }, [data])

    const handleInputChange = (e: any) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });        
    }

    const handleSubmit = () => {
        dispatch(submitUpdateTeamMemberFrom(formData));

        if (submitFormSuccess) {
            onSuccess();
        }
    }

    return (
        <>
        <div>
            <span className='title-text' >Dodawanie nowego członka zespołu</span>
            <p className='bottom-title-text'>Wypełnij wszystkie pola poniżej</p>
        </div>

        <form className='modal-form'>
            <div className='form-group'>
                <label>Name:</label>
                <input type="text" name="name" value={formData.name} onChange={handleInputChange} />
            </div>

            <div className='form-group'>
                <label>Email:</label>
                <input type="email" name="email" value={formData.email} onChange={handleInputChange} />
            </div>

            <div className='form-group'>
                <label>Phone:</label>
                <input type="text" name="phone" value={formData.phone} onChange={handleInputChange} />
            </div>
            
            <button onClick={handleSubmit} className='submit-button' disabled={submitFormLoading}>
                {submitFormLoading ? 'Submitting...' : 'Submit'}
            </button>

            {submitFormSuccess && <p>Form submitted successfully!</p>}
            {submitFormError && <p>Error: {submitFormError}</p>}
        </form>
        </>
    );
}

export default UpdateTeamMemberForm;