export interface UpdateTeamMemberFormData {
    id: number;
    name: string;
    email: string;
    phone: string;   
} 