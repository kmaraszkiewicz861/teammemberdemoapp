const initialState = {
    formData: {},
    submitFormLoading: false,
    submitFormSuccess: null,
    submitFormError: null,
  };

  const submitUpdateTeamMemberFormReducer = (state = initialState, action: any) => {
    switch (action.type) {
      case 'SUBMIT_FORM_REQUEST':
        return {
          ...state,
          submitFormLoading: true,
          submitFormSuccess: null,
          submitFormError: null,
        };
      case 'SUBMIT_FORM_SUCCESS':
        return {
          ...state,
          submitFormLoading: false,
          submitFormSuccess: action.payload,
        };
      case 'SUBMIT_FORM_ERROR':
        return {
          ...state,
          submitFormLoading: false,
          submitFormError: action.payload,
        };
      default:
        return state;
    }
  };

  export default submitUpdateTeamMemberFormReducer;