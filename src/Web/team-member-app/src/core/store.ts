import { configureStore } from '@reduxjs/toolkit';
import { teamMemberReducer, changeMemberStatusReducer } from '../features/team-members-list/team-member-reducers';
import submitAddTeamMemberFormReducer from '../features/add-team-member/add-team-member-form-reducers';
import submitUpdateTeamMemberFormReducer from '../features/update-team-member/update-team-member-form-reducers';

const store = configureStore({
    reducer: {
        teamMembers: teamMemberReducer,
        submitAddTeamMemberForm: submitAddTeamMemberFormReducer,
        submitUpdateTeamMemberForm: submitUpdateTeamMemberFormReducer,
        changeMemberStatus: changeMemberStatusReducer,
    },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;

export default store;