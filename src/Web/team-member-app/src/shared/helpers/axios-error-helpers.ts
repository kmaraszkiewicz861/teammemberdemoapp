import { AxiosError } from "axios";
import { ErrorResponse } from "../types/response-types";

export const getErrorFromAxiosResponse = (error: any): string[] => {
    if (error instanceof AxiosError && error.response) {
        const axiosError = error.response.data as ErrorResponse;

        return axiosError.errorMessages;
    }

    return [ (error as { message: string }).message ];
}