import { ReactNode } from "react";
import './modal.scss';

interface ModalProps {
    show: boolean;
    handleClose: () => void;
    children: ReactNode
}

const Modal = ({ handleClose, show, children }: ModalProps) => {
    const displayStyleValue = show ? 'modal-container display-block;' : 'modal-container display-none';
  
    return (
      <div className={displayStyleValue}>
        <section className="modal-container-main">
          {children}
          <button className='close-button' onClick={handleClose}>Ukryj</button>
        </section>
      </div>
    );
  };

export default Modal;