import { SUBMIT_FORM_ERROR, SUBMIT_FORM_REQUEST, SUBMIT_FORM_SUCCESS } from "../types/shared-consts";

export const submitFormRequest = () => ({
    type: SUBMIT_FORM_REQUEST,
});
  
export const submitFormSuccess = (data: any) => ({
    type: SUBMIT_FORM_SUCCESS,
    payload: data,
});
  
export const submitFormError = (error: string[]) => ({
    type: SUBMIT_FORM_ERROR,
    payload: error,
});