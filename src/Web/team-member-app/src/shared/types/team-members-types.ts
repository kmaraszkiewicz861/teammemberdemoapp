export interface TeamMemeber {
    id: number;
    name: string;
    email: string;
    phone: string;
    isActive: boolean;
    created: string
}

export interface ChangeMemberState {
    id: number;
    isActive: boolean;
}