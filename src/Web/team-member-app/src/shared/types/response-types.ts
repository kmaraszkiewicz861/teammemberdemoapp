export interface ErrorResponse {
    errorMessages: string[];
    isValid: boolean;
}