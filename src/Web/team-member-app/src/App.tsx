import React from 'react';
import './App.css';
import { Provider } from 'react-redux';
import store from './core/store'
import TeamMembersList from './features/team-members-list/team-member-list'

function App() {
  return (
    <Provider store={store}>
        <div className='App'>
          <TeamMembersList />
        </div>
    </Provider>
  );
}

export default App;
