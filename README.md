# TeamMemberDemoApp

## Uruchamianie api

1. Projekt można uruchomić z pliku:
   src/TeamMembers.sln
2. Do odpalenie migracji trzeba zmienić klase
   TeamMembers.Infrastructure.Database.TeamMembersDbContextFactory
   zmienić connectionstring w linii 11

Wiem, że takie rzeczy przechowywuje się w appsetings.json, jednak nie starczyło mi czasu na to 3. Możemy z lokalizacji src\API\TeamMembers.Infrastructure uruchomić komendę dotnet ef database update i powinna zostać stworzona baza danych 4. Dodatkowo trzeba zmienić connection string w appsetting.json znajdujący się: src\API\TeamMembers.Api\appsettings.json

## Uruchamianie ui

1. Projekt UI można odpalić w lokalizacji: src\Web\team-member-app
2. Komendami npm install oraz npm start

## Uwagi:

Niestety nie udało mi się ukończyć w 100% zadania, jest tego za dużo a ja osobiście nie będę miał przez parę tygodni czasu na dokończenie zadania w pełni. Podaje jednak link to repozytorium, jeżeli niedokończone zadanie mnie dyskwalifikuje, to rozumiem, ale jak pisze wolę oddać niekompletne niż w ogóle.

Czego nie udało się zrobić:

1. Importu danych z internetu
2. Szczegółów użytkownika
3. Style nie są dokończone, tutaj też zabrakło czasu, próbowałem to zakończyć dzisiaj jednak nie udało się
